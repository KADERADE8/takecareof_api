<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Cafigue  Fiche Medicale</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <link href="../" rel="stylesheet">
    </head>
    <style>
        --------------------------------------------------------------*/
body {
    font-family: 'Poppins';
    color: #272829;
  }
  
  a {
    color: #149ddd;
    text-decoration: none;
  }
  :root {
  --swiper-theme-color: #007aff;
}
  a:hover {
    color: #37b3ed;
    text-decoration: none;
  }
  
  h1,
  h2,
  h3,
  h4,
  h5,
  h6 {
    font-family: "Raleway", sans-serif;
  }
  
  
  /*--------------------------------------------------------------
  # Sections General
  --------------------------------------------------------------*/
  section {
    padding: 60px 0;
    overflow: hidden;
  }
  
  .section-bg {
    background: #f5f8fd;
  }
  
  .section-title {
    padding-bottom: 30px;
    background-color: #D6FFF3;
  }
  
  .section-title h2 {
    font-size: 32px;
    font-weight: bold;
    margin-bottom: 20px;
    padding-bottom: 20px;
    position: relative;
    color: #173b6c;
  }
  
  .section-title h2::after {
    content: "";
    position: absolute;
    display: inline-table;
    width: 50px;
    height: 3px;
    background: #149ddd;
    bottom: 0;
    left: 0;
  }
  
  .section-title p {
    margin-bottom: 0;
   
    
  }
  



  
  @-webkit-keyframes animate-loading {
    0% {
      transform: rotate(0deg);
    }
  
    100% {
      transform: rotate(360deg);
    }
  }
  
  @keyframes animate-loading {
    0% {
      transform: rotate(0deg);
    }
  
    100% {
      transform: rotate(360deg);
    }
  }
  

  /*--------------------------------------------------------------
  # Footer
  --------------------------------------------------------------*/
  #footer {
    padding: 15px;
    color: #fdfdfd;
    font-size: 14px;
    position: fixed;
    left: 0;
    bottom: 0;
    width: 300px;
    z-index: 9999;
    background: #04c3f8;
  }
 
  .img-navbar{
    width:20%;
  }
  #footer .copyright {
    text-align: center;
  }
  
  #footer .credits {
    padding-top: 5px;
    text-align: center;
    font-size: 13px;
    color: #eaebf0;
  }
  .items p{
    color:#00AEEB;
    font-weight: bolder;
  }
  
  @media (max-width: 1199px) {
    #footer {
      position: static;
      width: auto;
      padding-right: 20px 15px;
    }
  }
    </style>
    <body>
    <nav class="navbar navbar-expand-lg navbar-light " >
    <img src="/cafigue-01.png" class="img-navbar">
  <a class="navbar-brand" href="#"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse navabars" id="navbarSupportedContent" >
  @foreach ($mafiche as $mafiches)
    <div class="btn btn-primary">
    {{$mafiches->telephone}}
    </div>
  @endforeach
  </div>
</div>
</nav>
       
        <div class="container">
            <div class="section-title shadow p-3 mb-5  rounded">
                <h2>Fiche Medicale</h2>
                <p>Cette fiche medical vous permet d'intervenir le plus rapidement posible. veuillez à garder ces informations secretement
            </p>
            </div>
            <div class="alert alert-danger" role="alert">
              Contacter son parent proche le plutôt possible
            </div>
        @foreach ($mafiche as $mafiches)
        <div class="row shadow-none p-3 mb-5  rounded items">
            <div class="col-6 col-sm-4 shadow p-3 mb-5  rounded item">
            <iconify-icon icon="bi:geo-alt" style="color: #00AEEB;"></iconify-icon>
                 Adresse<p class="d-flex justify-content-center align-items-center">{{ $mafiches->Personne_vulnerable->adresse }}</p>
            </div>
            <div class="col-6 col-sm-4 shadow p-3 mb-5  rounded item">
            <iconify-icon icon="ant-design:user-outlined" style="color: #00AEEB;"></iconify-icon>
                Nom<p class="d-flex justify-content-center align-items-center">{{ $mafiches->Personne_vulnerable->nom }}</p>
            </div>
            <div class="col-6 col-sm-4 shadow p-3 mb-5  rounded item"> 
            <iconify-icon icon="carbon:phone-voice" style="color: #00AEEB;"></iconify-icon>
                  Parent<p class="d-flex justify-content-center align-items-center">{{ $mafiches->contact_personne_proche }}</p>
            </div> 
            <div class="col-6 col-sm-4 shadow p-3 mb-5  rounded item"> 
                <iconify-icon icon="game-icons:ages" style="color: #00aeeb;"></iconify-icon>
                  Âge
                  <p class="d-flex justify-content-center align-items-center">{{ $mafiches->Personne_vulnerable->age}} ans</p>
            </div> 
            <div class="col-6 col-sm-4 shadow p-3 mb-5  rounded item"> 
            <iconify-icon icon="simple-icons:transportforlondon" style="color: #00aeeb;"></iconify-icon>
                  Poids<p class="d-flex justify-content-center align-items-center">{{ $mafiches->poids }}</p>
            </div> 
            <div class="col-6 col-sm-4 shadow p-3 mb-5  rounded item"> 
            <iconify-icon icon="line-md:arrow-long-diagonal" style="color: #00aeeb;"></iconify-icon>
                  Taille:<p class="d-flex justify-content-center align-items-center">{{ $mafiches->taille }}</p>
            </div> 
            <div class="col-6 col-sm-4 shadow p-3 mb-5  rounded item"> 
            <iconify-icon icon="akar-icons:health" style="color: #00aeeb;"></iconify-icon>
                  Antécedent<p class="d-flex justify-content-center align-items-center">{{ $mafiches->probleme_medicale }}</p>
            </div> 
            <div class="col-6 col-sm-4 shadow p-3 mb-5  rounded item"> 
            <iconify-icon icon="bxs:analyse" style="color: #00aeeb;"></iconify-icon>
                  Traitement<p class="d-flex justify-content-center align-items-center">{{ $mafiches->traitement }}</p>
            </div> 
            <div class="col-6 col-sm-4 shadow p-3 mb-5  rounded item"> 
            <iconify-icon icon="fluent-emoji-flat:drop-of-blood" style="color: #EB0E00;"></iconify-icon>
                  Groupe sanguin<p class="d-flex justify-content-center align-items-center">{{ $mafiches->groupe_sanguin }}</p>
            </div> 
            </div>
            @endforeach
         </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    </body>
    <section class="">
  <!-- Footer -->
  <footer class="text-center text-white" style="background-color: #00AEEB;">
    <!-- Grid container -->
    <div class="container p-4 pb-0">
      <!-- Section: CTA -->
      <section class="">
        <p class="d-flex justify-content-center align-items-center">
          <span class="me-3">Inquiètez-vous de l'état de santé de votre parent ?</span>
          <button type="button" class="btn btn-outline-light btn-rounded">
           commandé ici
          </button>
        </p>
      </section>
      <!-- Section: CTA -->
    </div>
    <!-- Grid container -->

    <!-- Copyright -->
    <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
      © 2022 Copyright:
      <a class="text-white" href="https://www.cafigue.com">Cafigue</a>
    </div>
    <!-- Copyright -->
  </footer>
  <!-- Footer -->
</section>
<script src="https://code.iconify.design/iconify-icon/1.0.0/iconify-icon.min.js"></script>
</html>