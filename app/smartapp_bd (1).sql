-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:8889
-- Généré le : dim. 02 oct. 2022 à 17:40
-- Version du serveur :  5.7.34
-- Version de PHP : 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `smartapp_bd`
--

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `categorie` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `categories`
--

INSERT INTO `categories` (`id`, `categorie`) VALUES
(1, 'cdi'),
(2, 'cdd');

-- --------------------------------------------------------

--
-- Structure de la table `employer`
--

CREATE TABLE `employer` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `prenom` varchar(255) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `matricule` varchar(6) DEFAULT NULL,
  `datenaisse` date DEFAULT NULL,
  `lieudNaissance` varchar(55) DEFAULT NULL,
  `telephone` varchar(10) DEFAULT NULL,
  `id_entreprise` int(11) DEFAULT NULL,
  `id_categorie` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `employer`
--

INSERT INTO `employer` (`id`, `nom`, `prenom`, `email`, `matricule`, `datenaisse`, `lieudNaissance`, `telephone`, `id_entreprise`, `id_categorie`) VALUES
(4, 'zack', 'zoozo', 'zozo@gmail.com', 's01850', '2022-03-02', 'abidjan', '0142255270', 1, 1),
(5, 'zozo', 'toto', 'toto@gmail.com', '123DR', '2022-03-17', 'abidjan', '0142255270', 1, 2),
(6, 'zokou', 'yere', 'yere@gmail.com', 's01850', '2022-03-24', 'abidjan', '0142255270', 1, 2),
(7, 'zilatane', 'ouan', 'ouan@gmail.com', '123DS', '2022-03-25', 'abidjan', '0142255270', 1, 2),
(8, 'badoma', 'ouan', 'ouan@gmail.com', '123DK', '2022-03-25', 'abidjan', '0142255270', 1, 2),
(9, 'kone', 'sidiki', 'sidiki@gmail.com', '123Ds', '2022-03-25', 'abidjan', '0142255270', 1, 2),
(10, 'ballo', 'sidiki', 'sidikiballo@gmail.com', '123DS', '2022-03-25', 'abidjan', '0142255270', 1, 2),
(11, 'ali', 'bakary', 'alibakary@gmail.com', 's01855', '2022-03-02', 'abidjan', '0542255270', 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `entreprise`
--

CREATE TABLE `entreprise` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `description` text,
  `siteweb` varchar(50) DEFAULT NULL,
  `line_1` varchar(50) DEFAULT NULL,
  `line_2` varchar(50) DEFAULT NULL,
  `pays` varchar(50) DEFAULT NULL,
  `ville` varchar(50) DEFAULT NULL,
  `zipcode` varchar(50) DEFAULT NULL,
  `statut` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `entreprise`
--

INSERT INTO `entreprise` (`id`, `nom`, `description`, `siteweb`, `line_1`, `line_2`, `pays`, `ville`, `zipcode`, `statut`) VALUES
(1, 'orange', 'digital ', 'www.codival.com', 'cocody, saint jean', 'cocody, lavie', 'côte d\'ivoire', 'abidjan', '01 B.P 01453, abidjan', 'SARL'),
(2, 'mtn', 'mtn cote d\'ivoire', 'www.mtn.com', 'cocody, saint jean', 'cocody, lavie', 'côte d\'ivoire', 'bouake', 'bp 01', 'SARL'),
(3, 'moov', 'mtn cote d\'ivoire', 'www.mtn.com', 'cocody, saint jean', 'cocody, lavie', 'côte d\'ivoire', 'bouake', 'bp 01', 'SARL'),
(4, 'sopa', 'mtn cote d\'ivoire', 'www.mtn.com', 'cocody, saint jean', 'cocody, lavie', 'côte d\'ivoire', 'bouake', 'bp 01', 'SARL');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(45) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `employer`
--
ALTER TABLE `employer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_categorie` (`id_categorie`),
  ADD KEY `fk_entreprise` (`id_entreprise`);

--
-- Index pour la table `entreprise`
--
ALTER TABLE `entreprise`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `employer`
--
ALTER TABLE `employer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT pour la table `entreprise`
--
ALTER TABLE `entreprise`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `employer`
--
ALTER TABLE `employer`
  ADD CONSTRAINT `fk_categorie` FOREIGN KEY (`id_categorie`) REFERENCES `categories` (`id`),
  ADD CONSTRAINT `fk_entreprise` FOREIGN KEY (`id_entreprise`) REFERENCES `entreprise` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
