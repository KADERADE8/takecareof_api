<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DataResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "temperature"=> $this->temperature,
            "nombre_pas"=>  $this->nombre_pas,
            "frequence_res"=>   $this->frequence_res,
            "temperature_ambiante"=>    $this->temperature_ambiante,
            "vitesse"=> $this->vitesse,
            "rythme_card"=> $this->rythme_card,
            "oxygenation"=> $this->oxygenation,
            "pression_arterielle"=> $this->pression_arterielle,
            "AttributionsId"=> $this->AttributionsId
        ];
    }
}
