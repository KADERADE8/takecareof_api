<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AttributionRessource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'chambre'=>$this->chambre->NomChambre,
            'reference' => $this->dispositif->reference,
            'details' => $this->dispositif->details,
            'telephone' => $this->dispositif->telephone,
            'Adresse_ip' => $this->dispositif->Adresse_ip,
            'status' => $this->dispositif->status,
        ];
    }
}
