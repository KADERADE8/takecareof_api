<?php

namespace App\Http\Controllers;

use App\Models\Attributions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\AttributionRessource;
use App\Models\Dispositif;
use App\Models\Chambre;
use Illuminate\Support\Facades\DB;


class AttributionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $attribution=AttributionRessource::collection(Attributions::with('dispositif','chambre')->get());
 
        if(is_null($attribution) === true){
            return response()->json(array('Message' => " Collection vide !"), 200)->view();
        }else{
            return $attribution;
        }

    }

    public function count(){
        
        return response()->json([
            "nombre"=>Attributions::get()->count()
        ]);
    }

    public function countDispositifActif(){
        $total=DB::table('attributions')
                ->join('dispositif', 'dispositif.id','=','attributions.NumDispositif')
                ->select(DB::raw('count(*) as nombre'))
                ->where('dispositif.status','=','connecter')
                ->get();
                $total =$total[0]->nombre;
        return response()->json([
            'nombre'=>$total
        ]);
    }
    public function countDispositifNonActif(){
        $total=DB::table('attributions')
                ->join('dispositif', 'dispositif.id','=','attributions.NumDispositif')
                ->select(DB::raw('count(*) as nombre'))
                ->where('dispositif.status','=','deconnecter')
                ->get();
                $total =$total[0]->nombre;
        return response()->json([
            'nombre'=>$total
        ]);
    }

    public function countbyAttribution(){
        $nombre=DB::table('attributions')
                ->join('chambres', 'chambres.id', '=', 'attributions.NumChambre')
                ->join('dispositif', 'dispositif.id', '=', 'attributions.NumDispositif')
                ->select(DB::raw('count(distinct(NumChambre)) as nombre'))
                ->get();
                $nombre = $nombre[0]->nombre;
            
        return response()->json([
            'nombre'=>$nombre
        ]);
           
    }

    public function countChambreNonOccupe(){
        $nombre=DB::table('attributions')
                ->join('chambres', 'chambres.id', '=', 'attributions.NumChambre')
                ->join('dispositif', 'dispositif.id', '=', 'attributions.NumDispositif')
                ->select(DB::raw('count(distinct(NumChambre)) as nombre'))
                ->get();
                $nombre = $nombre[0]->nombre;

        $totale=Chambre::get()->count();
        $chambreNonOccupe=$totale-$nombre;
            
        return response()->json([
            'nombre'=>$chambreNonOccupe
        ]);
           
    }
 

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $req = $request->all();
    
        $validation =  Validator::make($req, [
            'reference' => 'required|exists:dispositif,reference',
            "Numchambre" => 'required|exists:chambres,id'
        ], $messages = [
            'required' => ':attribute est un champ obligatoire.',
            'exists' => 'n\'existe pas',
            
            
        ]);
        //dd($req);
        if ($validation->fails()) {
            return response()->json(['Erreur de validation' => $validation->errors()]);
        }

        $dispositif = Dispositif::where("reference","=",$request->reference)->get();
        foreach ($dispositif as $dispo){
            $disp = $dispo->id;
        }



        $attribution = [
            "NumDispositif"=>$disp,
            "Numchambre"=>$request->Numchambre
        ];

        $validation =  Validator::make($attribution, [
            'NumDispositif' => 'required|unique:attributions',
            "Numchambre" => 'required|exists:chambres,id'
        ], $messages = [
            'required' => ':attribute est un champ obligatoire.',
            'exists' => 'n\'existe pas',
            
            
        ]);
        //dd($req);
        if ($validation->fails()) {
            return response()->json(['Erreur de validation' => $validation->errors()]);
        }

        if (Attributions::create($attribution)) {
            return response()->json(array('Message' => "Enregistré avec succès !"), 200);
        } else {
            return response()->json(array('Message' => "Erreur d'enregistrement"));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $attribution=Attributions::with('dispositif','chambre')->select('*')->distinct()->where('NumChambre', $id)->get();
        if (!is_null($attribution)) {
            return $attribution;
        } else {
            return response()->json(['message' => 'introuvable']);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $attribution = Attributions::find($id);
        if (is_null($attribution)) {
            return response()->json(array('status' => 'false','ID introuvable'));
        } else {
            if ($attribution->delete()) {
                return response()->json(array('status' => 'true', 'success' => " Attribution Supprimée avec succès."), 200);
            } else {
                return response()->json(array('status' => 'false', 'erreur' => "Erreur de suppression "));
            }
        }
    }
}
