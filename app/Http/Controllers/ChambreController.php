<?php

namespace App\Http\Controllers;

use App\Models\Chambre;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class ChambreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $chambre = Chambre::all();
        return $chambre;
    }


    public function count(){
        
        return response()->json([
            "nombre"=>Chambre::get()->count()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validation = Validator::make($input, [
            "NomChambre" => 'required|max:255',
        ], [
            'required' => ':attribute est un champ obligatoire.',
        ]);
        if ($validation->fails()) {
            return response()->json(['Erreur de validation' => $validation->errors()]);
        }

        if (Chambre::create($request->all())) {
            return response()->json(array('Message' => "Enregistré avec succès !"), 200);
        } else {
            return response()->json(array('Message' => "Erreur d'enregistrement"));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $chambre = Chambre::find($id);
        if (!is_null($chambre)) {
            return $chambre;
        } else {
            return response()->json(['message' => 'introuvable']);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $chambre = Chambre::find($id);
        if (!is_null($chambre)) {
            $input = $request->all();
            $validation = Validator::make($input, [
                "NomChambre" => 'required|max:255',
            ], [
                'required' => ':attribute est un champ obligatoire.',
            ]);
            if ($validation->fails()) {
                return response()->json(['Erreur de validation' => $validation->errors()]);
            }
            if ($chambre->update($input)) {
                return response()->json(['message' => 'Modifié avec succès']);
            }
        } else {
            return response()->json(['message' => 'introuvable']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $chambre = Chambre::find($id);
        if (!is_null($chambre)) {
            return $chambre->delete();
        } else {
            return response()->json(['message' => 'introuvable']);
        }
    }
}
