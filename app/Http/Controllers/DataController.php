<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Data;
use App\Http\Resources\DataResource;

class DataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $data=Data::all();
        if(is_null($data) === true){
            return response()->json(array('Message' => " data  vide !"), 200)->view();
        }else{
            return $data;
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = [
            'temperature'=>$request->temperature,
            'nombre_pas'=>$request->nombre_pas,
            'frequence_res'=> $request->frequence_res,
            'temperature_ambiante'=> $request->temperature_ambiante,
            'vitesse'=> $request->vitesse,
            'rythme_card'=> $request->rythme_card,
            'oxygenation'=> $request->oxygenation,
            'pression_arterielle'=> $request->pression_arterielle,
            'AttributionsId'=> $request->AttributionsId
        ];

        if (Data::create($input)) {
            return response()->json(array('status' => 'true', 'success' => "Constante enregistrée"), 200);
        } else {
            return response()->json(array('status' => 'false', 'Erreur d\'enregistrement'));
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Data::find($id);
        if (is_null($data)) {
            return response()->json(array('ID incorrect'));
        } else {
            return $data;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Data::find($id);
        if (is_null($data)) {
            return response()->json(array('ID incorrect'));
        } else {

            if($data->update($request->all())){
                return response()->json(array('Message'=>"Mis à jour"));
            }
            else{
                return response()->json(array('Message'=>"Erreur"));
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Data::find($id);
        if (is_null($data)) {
            return response()->json(array('ID incorrect'));
        } else {
            if($data->delete()){
                return response()->json(array('Message'=>"Supprimé !"));
            }
            else{
                return response()->json(array('Message'=>"Erreur"));
            }
        }
    }

    public function databyDay(){
        $data=DB::table('data')
                ->select('*')
                ->get();
                
        return $data;
     
    }
}
