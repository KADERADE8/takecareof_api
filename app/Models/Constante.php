<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Constante
 * 
 * @property int $id
 * @property float|null $temperature
 * @property float|null $nombre_pas
 * @property float|null $frequence_res
 * @property float|null $temperature_ambiante
 * @property float|null $vitesse
 * @property float|null $rythme_card
 * @property float|null $oxygenation
 * @property float|null $pression_arterielle
 * @property float|null $pression_arterielle_systolique
 * @property Carbon $date
 * @property int|null $id_assigner
 *
 * @package App\Models
 */
class Constante extends Model
{
	protected $table = 'constante';
	public $timestamps = false;

	protected $casts = [
		'temperature' => 'float',
		'nombre_pas' => 'float',
		'frequence_res' => 'float',
		'temperature_ambiante' => 'float',
		'vitesse' => 'float',
		'rythme_card' => 'float',
		'oxygenation' => 'float',
		'pression_arterielle' => 'float',
		'pression_arterielle_systolique' => 'float',
		'id_assigner' => 'int'
	];

	protected $dates = [
		'date'
	];

	protected $fillable = [
		'temperature',
		'nombre_pas',
		'frequence_res',
		'temperature_ambiante',
		'vitesse',
		'rythme_card',
		'oxygenation',
		'pression_arterielle',
		'pression_arterielle_systolique',
		'date',
		'id_assigner'
	];
	public function assigner()
	{
		return $this->belongsTo(Assigner::class, 'id_assigner');
	}

}
