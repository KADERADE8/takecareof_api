<?php

namespace App\Models;

use App\Models\Chambre;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Attributions extends Model
{
    use HasFactory;

    protected $fillable = ['NumDispositif','Numchambre'];


    public function dispositif()
	{
		return $this->belongsTo(Dispositif::class, 'NumDispositif');
	}

	public function chambre()
	{
		return $this->belongsTo(Chambre::class, 'Numchambre');
	}

    public function data()
	{
		return $this->hasMany(Data::class, 'AttributionsId');
	}

}
