<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Chambre extends Model
{
    use HasFactory;
    protected $fillable = [
        'NomChambre'
    ];

    public function attribution()
	{
		return $this->hasMany(Chambre::class, 'Numchambre');
	}
}
