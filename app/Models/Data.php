<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Data
 * 
 * @property int $id
 * @property float|null $temperature
 * @property float|null $nombre_pas
 * @property float|null $frequence_res
 * @property float|null $temperature_ambiante
 * @property float|null $vitesse
 * @property float|null $rythme_card
 * @property float|null $oxygenation
 * @property float|null $pression_arterielle
 * @property float|null $pression_arterielle_systolique
 * @property int $AttributionsId
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * 
 * @property Attribution $attribution
 *
 * @package App\Models
 */
class Data extends Model
{
	protected $table = 'data';

	protected $casts = [
		'temperature' => 'float',
		'nombre_pas' => 'float',
		'frequence_res' => 'float',
		'temperature_ambiante' => 'float',
		'vitesse' => 'float',
		'rythme_card' => 'float',
		'oxygenation' => 'float',
		'pression_arterielle' => 'float',
		'pression_arterielle_systolique' => 'float',
		'AttributionsId' => 'int'
	];

	protected $fillable = [
		'temperature',
		'nombre_pas',
		'frequence_res',
		'temperature_ambiante',
		'vitesse',
		'rythme_card',
		'oxygenation',
		'pression_arterielle',
		'pression_arterielle_systolique',
		'AttributionsId'
	];

	public function attribution()
	{
		return $this->belongsTo(Attribution::class, 'AttributionsId');
	}
}
