<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DispositifController;
use App\Http\Controllers\FicheController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//  Route::get('/', [DispositifController::class, 'indexs']);
Route::get('qrcode/{id}', [DispositifController::class, 'generate'])->name('generate');
Route::get('ficheMedicale/{id}', [FicheController::class, 'fichemedical']);
Route::get('/navigate/back', [
    'uses' => 'RodrigoPedra\\HistoryNavigation\\Http\\Controllers\\HistoryNavigationController@back',
    'as' => 'navigate.back',
]);