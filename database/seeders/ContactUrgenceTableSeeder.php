<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ContactUrgenceTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('contact_urgence')->delete();
        
        \DB::table('contact_urgence')->insert(array (
            0 => 
            array (
                'id' => 6,
                'nom' => 'SDFG',
                'adresse' => 'ERTYUI',
                'telephone' => '345678',
            ),
            1 => 
            array (
                'id' => 8,
                'nom' => 'EHUI',
                'adresse' => 'ggg',
                'telephone' => '1424332341',
            ),
            2 => 
            array (
                'id' => 9,
                'nom' => 'EHUI',
                'adresse' => 'ggg',
                'telephone' => '142433234',
            ),
            3 => 
            array (
                'id' => 10,
                'nom' => 'EHUI',
                'adresse' => 'ggg',
                'telephone' => '142433234',
            ),
            4 => 
            array (
                'id' => 14,
                'nom' => 'ZANLE',
                'adresse' => 'Cocodyyy',
                'telephone' => '1234567899',
            ),
            5 => 
            array (
                'id' => 15,
                'nom' => 'EHUI',
                'adresse' => 'MAN',
                'telephone' => '1234567890',
            ),
            6 => 
            array (
                'id' => 16,
                'nom' => 'EHUI',
                'adresse' => 'MAN',
                'telephone' => '1234567890',
            ),
            7 => 
            array (
                'id' => 17,
                'nom' => 'EHUI',
                'adresse' => 'MAN',
                'telephone' => '1234567890',
            ),
            8 => 
            array (
                'id' => 18,
                'nom' => 'ZOULOU',
                'adresse' => 'DALAS',
                'telephone' => '1234567890',
            ),
            9 => 
            array (
                'id' => 19,
                'nom' => 'ZOULOU',
                'adresse' => 'DALAS',
                'telephone' => '1234567890',
            ),
            10 => 
            array (
                'id' => 20,
                'nom' => 'sami',
                'adresse' => 'macory',
                'telephone' => '1234537646',
            ),
            11 => 
            array (
                'id' => 21,
                'nom' => 'zozo1',
                'adresse' => 'Côte d\'Ivoire',
                'telephone' => '0142255270',
            ),
            12 => 
            array (
                'id' => 22,
                'nom' => 'zozo1',
                'adresse' => 'Côte d\'Ivoire',
                'telephone' => '0142255270',
            ),
            13 => 
            array (
                'id' => 23,
                'nom' => 'zozo1',
                'adresse' => 'Côte d\'Ivoire',
                'telephone' => '0709914074',
            ),
            14 => 
            array (
                'id' => 24,
                'nom' => 'zozo1',
                'adresse' => 'Côte d\'Ivoire',
                'telephone' => '0709914074',
            ),
            15 => 
            array (
                'id' => 25,
                'nom' => 'zozo1',
                'adresse' => 'Côte d\'Ivoire',
                'telephone' => '0709914074',
            ),
            16 => 
            array (
                'id' => 26,
                'nom' => 'zozo1',
                'adresse' => 'Côte d\'Ivoire',
                'telephone' => '0709914074',
            ),
            17 => 
            array (
                'id' => 27,
                'nom' => 'zozo1',
                'adresse' => 'Côte d\'Ivoire',
                'telephone' => '0709914074',
            ),
            18 => 
            array (
                'id' => 28,
                'nom' => 'zozo1',
                'adresse' => 'Côte d\'Ivoire',
                'telephone' => '0709914074',
            ),
            19 => 
            array (
                'id' => 29,
                'nom' => 'zozo1',
                'adresse' => 'Côte d\'Ivoire',
                'telephone' => '0142255270',
            ),
            20 => 
            array (
                'id' => 30,
                'nom' => 'zozo1',
                'adresse' => 'Côte d\'Ivoire',
                'telephone' => '0142255270',
            ),
            21 => 
            array (
                'id' => 31,
                'nom' => 'zozo1',
                'adresse' => 'Côte d\'Ivoire',
                'telephone' => '0142255270',
            ),
            22 => 
            array (
                'id' => 32,
                'nom' => 'zozo1',
                'adresse' => 'Côte d\'Ivoire',
                'telephone' => '0142255270',
            ),
            23 => 
            array (
                'id' => 33,
                'nom' => 'zozo1',
                'adresse' => 'Côte d\'Ivoire',
                'telephone' => '0142255270',
            ),
            24 => 
            array (
                'id' => 34,
                'nom' => 'zozo1',
                'adresse' => 'Côte d\'Ivoire',
                'telephone' => '0142255270',
            ),
            25 => 
            array (
                'id' => 35,
                'nom' => 'zozo1',
                'adresse' => 'Côte d\'Ivoire',
                'telephone' => '0142255270',
            ),
            26 => 
            array (
                'id' => 36,
                'nom' => 'EHUI',
                'adresse' => '123456789000',
                'telephone' => '0707103461',
            ),
            27 => 
            array (
                'id' => 37,
                'nom' => 'EHUI',
                'adresse' => 'ggg',
                'telephone' => '0142255270',
            ),
            28 => 
            array (
                'id' => 38,
                'nom' => 'EHUI',
                'adresse' => 'ggg',
                'telephone' => '0142255270',
            ),
            29 => 
            array (
                'id' => 39,
                'nom' => 'EHUI',
                'adresse' => 'ggg',
                'telephone' => '0142255270',
            ),
        ));
        
        
    }
}