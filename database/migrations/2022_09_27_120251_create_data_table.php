<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data', function (Blueprint $table) {
            $table->integer('id', true);
            $table->double('temperature')->nullable();
            $table->double('nombre_pas')->nullable();
            $table->double('frequence_res')->nullable();
            $table->double('temperature_ambiante')->nullable();
            $table->double('vitesse')->nullable();
            $table->double('rythme_card')->nullable();
            $table->double('oxygenation')->nullable();
            $table->double('pression_arterielle')->nullable();
            $table->double('pression_arterielle_systolique')->nullable();
            $table->integer('AttributionsId');
            $table->timestamps();

            $table->foreign('AttributionsId')
                ->references('id')
                ->on('attributions')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data');
    }
};
