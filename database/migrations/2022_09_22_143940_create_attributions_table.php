<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttributionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attributions', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('NumDispositif');
            $table->integer('Numchambre');
            $table->unique('NumDispositif');
            $table->timestamps();

            $table->foreign('NumDispositif')
                ->references('id')
                ->on('dispositif')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('Numchambre')
                ->references('id')
                ->on('chambres')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attributions');
    }
}
